# microsoft-TODO

> A [Microsoft To-Do Demo](https://capdiem.xyz/) created with Angular + Ngrx + Asp.net core
>
>

## Todo

- [ ] 待办事项可排序
- [x] 隐藏已完成待办事项
- [x] 导航栏每个代办清单名右边显示当前未完成数
- [x] 搜索(简易版)
- [x] 移动待办事项到其他待办清单
- [x] 待办事项今天到期
- [x] 待办事项明天到期
- [x] 待办事项删除截止日期
- [x] 待办事项添加note
- [x] 待办清单名第一个字符若是emoji则设置成图标
- [x] 双击待办事项显示详情
- [x] 右键待办事项弹出菜单
- [x] 右键清单弹出菜单
- [x] 点击清单标题修改，回车键或失去光标时保存修改
- [x] and so on...

## Issues
- [ ] 待办事项右键靠近边缘会被窗口限制
- [x] 添加到我的一天的事项第二天仍属于我的一天
- [x] 添加/删除待办事项导致所属待办清单的事项列表index混乱

## Build Setup

``` bash
cd client
# install dependencies
yarn
# run
yarn start
```