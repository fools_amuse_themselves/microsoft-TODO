import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { MyDayComponent } from './views/myDay/my-day.component'
import { TodoComponent } from './views/todo/todo.component'
import { SearchComponent } from './views/search/search.component'
import { HomeComponent } from './views/home/home.component'
import { LoginComponent } from './views/login/login.component'
import { AuthGuard } from './core/guards/auth.guard'

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'my-day',
        component: MyDayComponent
      },
      {
        path: 'lists/:id',
        component: TodoComponent
      },
      {
        path: 'search',
        component: SearchComponent
      }
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
