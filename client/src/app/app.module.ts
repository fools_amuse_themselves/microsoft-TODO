import { BrowserModule } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'

import { StoreModule } from '@ngrx/store'
import { StoreDevtoolsModule } from '@ngrx/store-devtools'
import { StoreRouterConnectingModule } from '@ngrx/router-store'
import { EffectsModule } from '@ngrx/effects'

import { NgZorroAntdModule } from 'ng-zorro-antd'
import { CookieService } from 'ng2-cookies'

import { FocusDirective } from './core/directives/focus.directive'

import { AppRoutingModule } from './app-routing.module'
import { AuthGuard } from './core/guards/auth.guard'

import { AppComponent } from './views/app.component'
import { HomeComponent } from './views/home/home.component'
import { LoginComponent } from './views/login/login.component'
import { SiderComponent } from './views/sider/sider.component'
import { SiderUserComponent } from './views/sider/user/user.component'
import { MyDayComponent } from './views/myDay/my-day.component'
import { TodoComponent } from './views/todo/todo.component'
import { SearchComponent } from './views/search/search.component'

import { reducers, metaReducers } from './core/reducers'
import { environment } from '../environments/environment'
import { UserEffects } from './core/effetcs/user.effect'
import { RenameTitleDirective } from './core/directives/rename-title.directive'
import { SiderContextmenuComponent } from './views/sider/contextmenu/contextmenu.component'
import { TodoContextmenuComponent } from './views/todo/contextmenu/contextmenu.component'
import { DetailComponent } from './views/todo/detail/detail.component'
import { RenderDeadlinePipe } from './core/pipes/renderDeadline.pipe'
import { Functions } from './common/functions'
import { ChecklistEffects } from './core/effetcs/checklist.effect'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    SiderComponent,
    SiderUserComponent,
    MyDayComponent,
    TodoComponent,
    SearchComponent,
    LoginComponent,
    FocusDirective,
    RenameTitleDirective,
    SiderContextmenuComponent,
    TodoContextmenuComponent,
    DetailComponent,
    RenderDeadlinePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    // 在根 module 中需要使用 NgZorroAntdModule.forRoot()，在子 module 需要使用 NgZorroAntdModule
    NgZorroAntdModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot([UserEffects, ChecklistEffects]),
    !environment.production
      ? StoreDevtoolsModule.instrument({ maxAge: 50 })
      : [],
    StoreRouterConnectingModule
  ],
  providers: [
    Functions,
    AuthGuard,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
