import { Action } from '@ngrx/store'
import { IList, ITask } from '../models'
import {
  INewListDto,
  INewTaskDto,
  IRenameTitleDto,
  IDeleteTaskDto,
  IUpdateTaskDto,
  IMoveTaskDto,
  IShowFinishedDto
} from '../dtos'

export enum ChecklistActionTypes {
  FetchChecklist = '[LIST] Fetch Checklist',
  FetchChecklistSuccess = '[LIST] Fetch Checklist Success',
  AddNewList = '[LIST] Add Checklist',
  AddNewListSuccess = '[LIST] Add Checklist Success',
  DeleteList = '[LIST] Delete Checklist',
  DeleteListSuccess = '[LIST] Delete Checklist Success',
  RenameListTitle = '[LIST] Rename Checklist',
  RenameListTitleSuccess = '[LIST] Rename Checklist Success',
  ToggleShowFinished = '[LIST] Toggle Show Finished Tasks',

  AddTodayTask = '[TASK] ADD Today Task',
  AddTodayTaskSuccess = '[TASK] Add Today Task Success',
  AddNewTask = '[TASK] Add Task',
  AddNewTaskSuccess = '[TASK] Aadd Task Success',
  DeleteTask = '[TASK] Delete Task',
  DeleteTaskSuccess = '[TASK] Delete Task Success',
  ToggleDoneTask = '[TASK] Toggle Done Task',
  ToggleDoneTaskSuccess = '[TASK] Toggle Done Task Success',
  ToggleMydayTask = '[TASK] Toggle Myday Task',
  ToggleMydayTaskInSuccess = '[TASK] Toggle Myday Task In Success',
  ToggleMydayTaskOutSuccess = '[TASK] Toggle Myday Task Out Success',
  RenameTask = '[TASK] Rename Task',
  RenameTaskSuccess = '[TASK] Rename Task Success',
  UpdateTaskNote = '[TASK] Update Task Note',
  UpdateTaskNoteSuccess = '[TASK] Update Task Note Success',
  SetBeDoneTomorrow = '[TASK] Set Be Done Tomorrow',
  SetBeDoneTomorrowSuccess = '[TASK] Set Be Done Tomorrow Success',
  ClearDeadline = '[Task] Clear Deadline',
  ClearDeadlineSuccess = '[Task] Clear Deadline Success',
  SetBeDoneToday = '[TASK] Set Be Done Today',
  SetBeDoneTodaySuccess = '[TASK] Set Be Done Today Success',
  MoveTask = '[TASK] Movi Task',
  MoveTaskSuccess = '[TASK] Move Success'
}

//#region [LIST]
export class FetchChecklistAction implements Action {
  readonly type = ChecklistActionTypes.FetchChecklist
  constructor (public payload: string) { }
}

export class FetchChecklistSuccessAction implements Action {
  readonly type = ChecklistActionTypes.FetchChecklistSuccess
  constructor (public payload: IList[]) { }
}

export class AddChecklistAction implements Action {
  readonly type = ChecklistActionTypes.AddNewList
  constructor (public payload: INewListDto) { }
}

export class AddChecklistSuccessAction implements Action {
  readonly type = ChecklistActionTypes.AddNewListSuccess
  constructor (public payload: IList) { }
}

export class DeleteChecklistAction implements Action {
  readonly type = ChecklistActionTypes.DeleteList
  constructor (public payload: string) { }
}

export class DeleteChecklistSuccessAction implements Action {
  readonly type = ChecklistActionTypes.DeleteListSuccess
  constructor (public payload: string) { }
}

export class RenameChecklistAction implements Action {
  readonly type = ChecklistActionTypes.RenameListTitle
  constructor (public payload: IRenameTitleDto) { }
}

export class RenameChecklistSuccessAction implements Action {
  readonly type = ChecklistActionTypes.RenameListTitleSuccess
  constructor (public payload: IRenameTitleDto) { }
}

export class ToggleShowFinishedAction implements Action {
  readonly type = ChecklistActionTypes.ToggleShowFinished
  constructor (public payload: IShowFinishedDto) { }
}

//#endregion

//#region [TASK]
export class DeleteTaskAction implements Action {
  readonly type = ChecklistActionTypes.DeleteTask
  constructor (public payload: IDeleteTaskDto) { }
}

export class DeleteTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.DeleteTaskSuccess
  constructor (public payload: IDeleteTaskDto) { }
}

export class AddTodayTaskAction implements Action {
  readonly type = ChecklistActionTypes.AddTodayTask
  constructor (public payload: INewTaskDto) { }
}

export class AddTodayTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.AddTodayTaskSuccess
  constructor (public payload: ITask) { }
}

export class AddNewTaskAction implements Action {
  readonly type = ChecklistActionTypes.AddNewTask
  constructor (public payload: INewTaskDto) { }
}

export class AddNewTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.AddNewTaskSuccess
  constructor (public payload: ITask) { }
}

export class ToggleDoneTaskActin implements Action {
  readonly type = ChecklistActionTypes.ToggleDoneTask
  constructor (public payload: IUpdateTaskDto) { }
}

export class ToggleDoneTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.ToggleDoneTaskSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class ToggleMydayTaskAction implements Action {
  readonly type = ChecklistActionTypes.ToggleMydayTask
  constructor (public payload: IUpdateTaskDto) { }
}

export class ToggleMydayTaskInSuccessAction implements Action {
  readonly type = ChecklistActionTypes.ToggleMydayTaskInSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class ToggleMydayTaskOutSuccessAction implements Action {
  readonly type = ChecklistActionTypes.ToggleMydayTaskOutSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class RenameTaskAction implements Action {
  readonly type = ChecklistActionTypes.RenameTask
  constructor (public payload: IUpdateTaskDto) { }
}

export class RenameTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.RenameTaskSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class UpdateTaskNoteAction implements Action {
  readonly type = ChecklistActionTypes.UpdateTaskNote
  constructor (public payload: IUpdateTaskDto) { }
}

export class UpdateTaskNoteSuccessAction implements Action {
  readonly type = ChecklistActionTypes.UpdateTaskNoteSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class SetBeDoneTomorrowAction implements Action {
  readonly type = ChecklistActionTypes.SetBeDoneTomorrow
  constructor (public payload: IUpdateTaskDto) { }
}

export class SetBeDoneTomorrowSuccessAction implements Action {
  readonly type = ChecklistActionTypes.SetBeDoneTomorrowSuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class SetBeDoneTodayAction implements Action {
  readonly type = ChecklistActionTypes.SetBeDoneToday
  constructor (public payload: IUpdateTaskDto) { }
}

export class SetBeDoneTodaySuccessAction implements Action {
  readonly type = ChecklistActionTypes.SetBeDoneTodaySuccess
  constructor (public payload: IUpdateTaskDto) { }
}

export class ClearDeadlineAction implements Action {
  readonly type = ChecklistActionTypes.ClearDeadline
  constructor (public payload: IDeleteTaskDto) { }
}

export class ClearDeadlineSuccessAction implements Action {
  readonly type = ChecklistActionTypes.ClearDeadlineSuccess
  constructor (public payload: IDeleteTaskDto) { }
}

export class MoveTaskAction implements Action {
  readonly type = ChecklistActionTypes.MoveTask
  constructor (public payload: IMoveTaskDto) { }
}

export class MoveTaskSuccessAction implements Action {
  readonly type = ChecklistActionTypes.MoveTaskSuccess
  constructor (public payload: IMoveTaskDto) { }
}

//#endregion [TASK]

export type ChecklistActions
  = FetchChecklistAction
  | FetchChecklistSuccessAction
  | AddChecklistAction
  | AddChecklistSuccessAction
  | DeleteChecklistAction
  | DeleteChecklistSuccessAction
  | RenameChecklistAction
  | RenameChecklistSuccessAction

  | AddTodayTaskAction
  | AddTodayTaskSuccessAction
  | AddNewTaskAction
  | AddNewTaskSuccessAction
  | DeleteTaskAction
  | DeleteTaskSuccessAction
  | ToggleDoneTaskActin
  | ToggleDoneTaskSuccessAction
  | ToggleMydayTaskAction
  | ToggleMydayTaskInSuccessAction
  | ToggleMydayTaskOutSuccessAction
  | RenameTaskAction
  | RenameTaskSuccessAction
  | UpdateTaskNoteAction
  | UpdateTaskNoteSuccessAction
  | SetBeDoneTomorrowAction
  | SetBeDoneTomorrowSuccessAction
  | SetBeDoneTodayAction
  | SetBeDoneTodaySuccessAction
  | ClearDeadlineAction
  | ClearDeadlineSuccessAction
  | MoveTaskAction
  | MoveTaskSuccessAction
  | ToggleShowFinishedAction
