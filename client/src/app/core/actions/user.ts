import { Action } from '@ngrx/store'
import { IUser } from '../../core/models'
import { IError } from '../models/error'
import { IRegister } from '../models/register'

export enum UserActionTypes {
  Login = '[USER] Login',
  Logout = '[USER] Logout',
  LoginSuccess = '[USER] Login Success',
  LoginFailure = '[USER] Login Failure',
  Register = '[USER] Register',
  RegisterSuccess = '[USER] Register Success',
  RegisterFailure = '[USER] Register Failure',
  FetchCurrentUser = '[USER] Fetch Current User',
  FetchUserSuccess = '[USER] Fetch Current User Success',
  RefreshTokenSuccess = '[USER] Refresh Token Success'
}

export class RefreshTokenSuccessAction implements Action {
  readonly type = UserActionTypes.RefreshTokenSuccess
  constructor (public payload: any) { }
}

export class FetchCurrentUserAction implements Action {
  readonly type = UserActionTypes.FetchCurrentUser
}

export class FetchUserSuccessAction implements Action {
  readonly type = UserActionTypes.FetchUserSuccess
  constructor (public payload: IUser) { }
}

export class RegisterAction implements Action {
  readonly type = UserActionTypes.Register
  constructor (public payload: IRegister) { }
}

export class RegisterSuccessAction implements Action {
  readonly type = UserActionTypes.RegisterSuccess
}

export class RegisterFailureAction implements Action {
  readonly type = UserActionTypes.RegisterFailure
  constructor (public payload: any) { }
}

export class LoginAction implements Action {
  readonly type = UserActionTypes.Login
  constructor (public payload: object) { }
}

export class LogoutAction implements Action {
  readonly type = UserActionTypes.Logout
}

export class LoginSuccessAction implements Action {
  readonly type = UserActionTypes.LoginSuccess
  constructor (public payload: any) { }
}

export class LoginFailureAction implements Action {
  readonly type = UserActionTypes.LoginFailure
  constructor (public payload: IError) { }
}

export type UserActions
  = LoginSuccessAction
  | LoginFailureAction
  | LoginAction
  | LogoutAction
  | RegisterAction
  | RegisterSuccessAction
  | RegisterFailureAction
  | FetchCurrentUserAction
  | FetchUserSuccessAction
  | RefreshTokenSuccessAction
