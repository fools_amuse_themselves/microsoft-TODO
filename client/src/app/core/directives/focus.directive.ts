import { Directive, Input, EventEmitter, ElementRef, Renderer, Inject, OnInit } from '@angular/core'

@Directive({
  selector: '[appFocus]'
})
export class FocusDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('appFocus') focusEvent: EventEmitter<boolean>
  constructor (
    @Inject(ElementRef) private _element: ElementRef,
    private _renderer: Renderer) { }

  ngOnInit () {
    if (this.focusEvent) {
      this.focusEvent.subscribe((event) => {
        this._renderer.invokeElementMethod(this._element.nativeElement, 'focus', [])
      })
    }
  }
}
