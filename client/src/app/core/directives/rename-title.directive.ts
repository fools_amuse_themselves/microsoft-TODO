import { Directive, Input, EventEmitter, ElementRef, Renderer, Inject, OnInit } from '@angular/core'

@Directive({
  selector: '[appRenameTitle]'
})
export class RenameTitleDirective implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('appRenameTitle') selectEvent: EventEmitter<any>
  constructor (
    @Inject(ElementRef) private _element: ElementRef,
    private _renderer: Renderer) { }

  ngOnInit () {
    this.selectEvent.subscribe((event) => {
      this._renderer.setElementStyle(this._element.nativeElement, 'display', 'block')
      this._renderer.invokeElementMethod(this._element.nativeElement, 'select', [])
    })
  }
}
