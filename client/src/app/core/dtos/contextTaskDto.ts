import { ITask } from '../models'

// 用于待办事项右键 比Task类型多一个listId 便于操作
export interface IContextTaskDto extends ITask {
  listId: string
}
