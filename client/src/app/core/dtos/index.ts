import { IContextTaskDto } from './contextTaskDto'
import { IDeleteTaskDto } from './deleteTaskDto'
import { IMoveTaskDto } from './moveTaskDto'
import { INewListDto } from './newListDto'
import { INewTaskDto } from './newTaskDto'
import { IRenameTitleDto } from './renameTitleDto'
import { IUpdateTaskDto } from './updateTaskDto'
import { IShowFinishedDto } from './listShowFinished'

export {
  IContextTaskDto,
  IDeleteTaskDto,
  IMoveTaskDto,
  INewListDto,
  INewTaskDto,
  IRenameTitleDto,
  IUpdateTaskDto,
  IShowFinishedDto
}
