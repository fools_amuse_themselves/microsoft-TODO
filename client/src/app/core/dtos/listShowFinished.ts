export interface IShowFinishedDto {
  listId: string
  show: boolean
}
