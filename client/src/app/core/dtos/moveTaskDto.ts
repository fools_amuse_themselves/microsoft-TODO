export interface IMoveTaskDto {
  fromListId: string
  index: number
  toListId: string
}