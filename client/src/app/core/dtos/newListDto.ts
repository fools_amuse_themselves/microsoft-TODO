export interface INewListDto {
  title: string
  icon: string
  userId: string
}
