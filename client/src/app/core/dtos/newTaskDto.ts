export interface INewTaskDto {
  todo: string
  listId: string
}
