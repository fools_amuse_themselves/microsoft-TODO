export interface IRenameTitleDto {
  id: string
  title: string
  icon: string
}
