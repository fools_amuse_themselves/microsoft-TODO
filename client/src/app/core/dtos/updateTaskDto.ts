
export interface IUpdateTaskDto {
  listId: string
  index: number
  [propName: string]: any
}
