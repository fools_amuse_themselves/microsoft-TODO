import { ITask } from './task'

export interface IList {
  id: string
  title: string
  icon: string
  zindex: number
  number: number
  isStatic: boolean
  userId: string
  tasks: ITask[]
  unfinished: number
  showFinished: boolean
}
