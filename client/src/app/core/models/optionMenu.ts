export interface IOptionMenu {
  show: boolean
  type: number
  showFinished: boolean
}
