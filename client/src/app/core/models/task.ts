export interface ITask {
  todo: string           // 标题
  isDone: boolean        // 是否完成
  today: boolean         // 我的一天
  creationTime: string   // 创建时间
  reminderTime: string   // 提醒时间
  deadline: string       // 截至日期
  notes: string          // 备注

  listId: string         // 外键 所属待办清单
  listName: string       // 所属待办清单名称
  index: number          // 下标（只用于删除时）
}
