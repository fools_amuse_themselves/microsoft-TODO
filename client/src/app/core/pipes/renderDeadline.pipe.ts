import { Pipe, PipeTransform } from '@angular/core'
import * as moment from 'moment'

/**
 * 重置期限输出
 *
 * @export
 * @class RenderDeadlinePipe
 * @implements {PipeTransform}
 */
@Pipe({ name: 'renderDeadline' })
export class RenderDeadlinePipe implements PipeTransform {
  transform (value: string): string {
    const deadline = moment(value.substring(0, 10))
    const now = moment(moment().format('YYYY-MM-DD'))
    const diff = deadline.diff(now, 'd')
    if (diff === 0) {
      return '今天'
    } else if (diff === 1) {
      return '明天'
    } else if (diff < 0) {
      return deadline.format('MMMMDo dddd')
    } else if (diff > 0) {
      return deadline.format('MMMMDo dddd')
    }
  }
}
