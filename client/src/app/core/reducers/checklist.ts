import { createSelector, createFeatureSelector } from '@ngrx/store'
import { IList, ITask } from '../../core/models'
import { ChecklistActions, ChecklistActionTypes } from '../actions/checklist'

export interface IState {
  checklists: IList[]
}

export const initialState: IState = {
  checklists: []
}

export function reducer(
  state = initialState,
  action: ChecklistActions): IState {
  switch (action.type) {
    //#region Checklist
    case ChecklistActionTypes.FetchChecklistSuccess:
      return {
        ...state,
        checklists: action.payload.map((list) => {
          return {
            ...list,
            showFinished: true
          }
        })
      }
    case ChecklistActionTypes.AddNewListSuccess:
      return {
        ...state,
        checklists: [
          ...state.checklists,
          action.payload
        ]
      }
    case ChecklistActionTypes.DeleteListSuccess:
      const index = state.checklists.findIndex((u) => u.id === action.payload)
      state.checklists.splice(index, 1)
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    case ChecklistActionTypes.RenameListTitleSuccess:
      let checklist = state.checklists.find((u) => u.id === action.payload.id)
      checklist.icon = action.payload.icon
      checklist.title = action.payload.title
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    case ChecklistActionTypes.ToggleShowFinished:
      checklist = state.checklists.find((u) => u.id === action.payload.listId)
      checklist.showFinished = action.payload.show
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    //#endregion
    //#region Task
    case ChecklistActionTypes.AddTodayTaskSuccess:
      const todoList = state.checklists.find((u) => u.isStatic)
      action.payload.index = todoList.tasks.length
      action.payload.listId = todoList.id
      action.payload.listName = todoList.title
      todoList.tasks.push(action.payload)
      return {
        ...state,
        checklists: [...state.checklists]
      }
    case ChecklistActionTypes.AddNewTaskSuccess:
      const currentList = state.checklists.find((u) => u.id === action.payload.listId)
      action.payload.index = currentList.tasks.length
      currentList.tasks.push(action.payload)
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    case ChecklistActionTypes.DeleteTaskSuccess:
      // 从tasks中删除
      const tasks = state.checklists.find((u) => u.id === action.payload.listId).tasks
      const _index = tasks.findIndex((u) => u.index === action.payload.index)
      tasks.splice(_index, 1)
      tasks.map((item) => {
        if (item.index > action.payload.index) {
          item.index--
        }
      })
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    case ChecklistActionTypes.ToggleDoneTaskSuccess:
      let retState = wow(state, action.payload.listId, action.payload.index, 'isDone', action.payload.isDone)
      if (action.payload.isDone) {
        new Audio('../../../assets/complete.wav').play()
      }
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.ToggleMydayTaskInSuccess:
      retState = wow(state, action.payload.listId, action.payload.index, 'today', true)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.ToggleMydayTaskOutSuccess:
      retState = wow(state, action.payload.listId, action.payload.index, 'today', false)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.RenameTaskSuccess:
      retState = wow(state, action.payload.listId, action.payload.index, 'todo', action.payload.todo)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.UpdateTaskNoteSuccess:
      retState = wow(state, action.payload.listId, action.payload.index, 'notes', action.payload.notes)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.SetBeDoneTomorrowSuccess:
      let deadlineTemp = (action.payload.deadline as Date).toJSON()
      retState = wow(state, action.payload.listId, action.payload.index, 'deadline', deadlineTemp)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.SetBeDoneTodaySuccess:
      deadlineTemp = (action.payload.deadline as Date).toJSON()
      retState = wow(state, action.payload.listId, action.payload.index, 'deadline', deadlineTemp)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.ClearDeadlineSuccess:
      retState = wow(state, action.payload.listId, action.payload.index, 'deadline', null)
      return {
        ...state,
        checklists: [
          ...retState.checklists
        ]
      }
    case ChecklistActionTypes.MoveTaskSuccess:
      const { fromListId, index: mIndex, toListId } = action.payload
      const fromList = state.checklists.find((u) => u.id === fromListId)
      const toList = state.checklists.find((u) => u.id === toListId)
      // 添加到目标清单
      const mTask = fromList.tasks.find((u) => u.index === mIndex)
      mTask.index = toList.tasks.length
      toList.tasks.push(mTask)
      // 从原清单中删除
      const _mIndex = fromList.tasks.findIndex((u) => u.index === mIndex)
      fromList.tasks.splice(_mIndex, 1)
      fromList.tasks.map((item) => {
        if (item.index > mIndex) {
          item.index--
        }
      })
      return {
        ...state,
        checklists: [
          ...state.checklists
        ]
      }
    //#endregion
    default:
      return state
  }
}

export const selectChecklists = createFeatureSelector<IList[]>('checklists')
export const selectTodayTasks = createSelector(
  selectChecklists,
  (lists) => {
    // 设置today tasks
    const todays: ITask[] = []
    lists.forEach((list) => {
      const res = list.tasks.filter((task) => task.today)
      res.forEach((v) => {
        v.listId = list.id
        v.listName = list.title
      })
      todays.push(...res)
    })
    return todays
  }
)

// 更新指定task字段
function wow(state: IState, listId: string, index: number, field: string, value: any): IState {
  const task = state.checklists.find((u) => u.id === listId)
    .tasks.find((v) => v.index === index)
  task[field] = value
  return state
}
