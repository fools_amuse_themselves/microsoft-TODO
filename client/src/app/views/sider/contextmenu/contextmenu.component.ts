import { Component, Input } from '@angular/core'
import { NzModalService } from 'ng-zorro-antd'
import { Store } from '@ngrx/store'
import * as fromRoot from '../../../core/reducers'
import { DeleteChecklistAction } from '../../../core/actions/checklist'
import { IList } from '../../../core/models'

@Component({
  selector: 'app-sider-contextmenu',
  templateUrl: './contextmenu.component.html',
  styleUrls: ['./contextmenu.component.less']
})
export class SiderContextmenuComponent {
  @Input() currentList: IList
  @Input() visible: boolean
  @Input() x: number
  @Input() y: number

  constructor (
    private _confirm: NzModalService,
    private _store: Store<fromRoot.IState>) { }

  get locationCSS () {
    return {
      display: this.visible ? 'block' : 'none',
      left: this.x + 'px',
      top: this.y + 'px'
    }
  }

  deleteList () {
    const ctx = this
    this._confirm.confirm({
      title: `您确定要删除清单（${this.currentList.title}）吗？`,
      onOk () {
        ctx._store.dispatch(new DeleteChecklistAction(ctx.currentList.id))
      },
      // tslint:disable-next-line:no-empty
      onCancel () {}
    })
  }
}
