import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { Cookie } from 'ng2-cookies'
import { Store } from '@ngrx/store'
import { Observable } from 'rxjs/Observable'

import { IUser } from '../../../core/models'
import * as fromRoot from '../../../core/reducers'
import * as fromUserAction from '../../../core/actions/user'

@Component({
  selector: 'app-sider-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class SiderUserComponent implements OnInit {
  currentUser: IUser
  avatar: string
  isModalVisible = false
  constructor (
    private _router: Router,
    private _store: Store<fromRoot.IState>
  ) { }

  ngOnInit (): void {
    this._store.select(fromRoot.selectUserEntity).subscribe((user) => {
      this.currentUser = user
      if (!this.currentUser.avatar) {
        this.avatar = this.currentUser.nickname.slice(0, 1).toUpperCase()
      }
    })
  }

  handleClickSettings () {
    this.isModalVisible = true
  }

  handleCancel (e) {
    this.isModalVisible = false
  }

  logout () {
    this._store.dispatch(new fromUserAction.LogoutAction())
  }
}
