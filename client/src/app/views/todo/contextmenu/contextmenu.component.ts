import { Component, Input, OnChanges } from '@angular/core'
import * as moment from 'moment'
import { Router } from '@angular/router'
import { NzMessageService, NzModalService } from 'ng-zorro-antd'
import { Store } from '@ngrx/store'
import * as fromRoot from '../../../core/reducers'
import * as fromChecklist from '../../../core/actions/checklist'
import { IDeleteTaskDto, IUpdateTaskDto, IMoveTaskDto } from '../../../core/dtos'
import { ITask, IList } from '../../../core/models'
import { Functions } from '../../../common/functions'
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-todo-contextmenu',
  templateUrl: './contextmenu.component.html',
  styleUrls: ['./contextmenu.component.less']
})
export class TodoContextmenuComponent {
  @Input() currentTask: ITask
  @Input() visible: boolean
  @Input() x: number
  @Input() y: number
  lists: Observable<IList[]>

  constructor (
    private _router: Router,
    private _store: Store<fromRoot.IState>,
    private _funcs: Functions,
    private _message: NzMessageService) {
      this.lists = _store.select(fromRoot.selectChecklists)
    }

  get locationCSS () {
    return {
      display: this.visible ? 'block' : 'none',
      left: this.x + 'px',
      top: this.y + 'px'
    }
  }

  isDefaultIcon (icon): boolean {
    return this._funcs.isDefaultIcon(icon)
  }

  toggleToday () {
    const { listId, index, today } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      ['today']: !today
    }
    this._store.dispatch(new fromChecklist.ToggleMydayTaskAction(dto))
  }

  toggleDone () {
    const { listId, index, isDone } = this.currentTask
    const dto = {
      listId,
      index,
      ['isDone']: !isDone
    }
    this._store.dispatch(new fromChecklist.ToggleDoneTaskActin(dto))
  }

  toBeDoneToday () {
    const { listId, index } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      ['deadline']: moment().toDate()
    }
    this._store.dispatch(new fromChecklist.SetBeDoneTodayAction(dto))
  }

  toBeDoneTomorrow () {
    const { listId, index } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      ['deadline']: moment().add(1, 'd').toDate()
    }
    this._store.dispatch(new fromChecklist.SetBeDoneTomorrowAction(dto))
  }

  deleteDeadline () {
    const { listId, index } = this.currentTask
    const dto: IDeleteTaskDto = {
      listId,
      index
    }
    this._store.dispatch(new fromChecklist.ClearDeadlineAction(dto))
  }

  moveTo (toListId: string) {
    const dto: IMoveTaskDto = {
      fromListId: this.currentTask.listId,
      index: this.currentTask.index,
      toListId
    }
    this._store.dispatch(new fromChecklist.MoveTaskAction(dto))
  }

  deleteTask () {
    const { listId, index } = this.currentTask
    const deleteTask: IDeleteTaskDto = {
      listId: this.currentTask.listId,
      index: this.currentTask.index
    }
    this._store.dispatch(new fromChecklist.DeleteTaskAction(deleteTask))
  }

}
