import { Component, EventEmitter, Input, Output, OnChanges, HostListener } from '@angular/core'
import * as moment from 'moment'
import { Store } from '@ngrx/store'
import * as fromRoot from '../../../core/reducers'
import * as fromChecklist from '../../../core/actions/checklist'

import { ITask } from '../../../core/models'
import { IUpdateTaskDto } from '../../../core/dtos'
import { KEY_CODE } from '../../../common/keycodes'

@Component({
  selector: 'app-task-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.less']
})
export class DetailComponent implements OnChanges {
  @Input() currentTask: ITask
  @Output() onHideDetail = new EventEmitter<boolean>()
  creationTime: string
  isTitleClicked: boolean
  taskTitle: string
  notes: string
  isSaveNotesBtnClicked = false // 保存notes的确定按钮是否按下
  isNotesFocus = false  // notes textarea 是否被聚焦

  renameTitleEvent = new EventEmitter<any>()

  constructor (
    private _store: Store<fromRoot.IState>) {}

  ngOnChanges () {
    if (this.currentTask) {
      const temp = this.currentTask.creationTime
      this.creationTime = moment(temp).format('创建于 MMMMDo, dddd')
      this.taskTitle = this.currentTask.todo
      this.notes = this.currentTask.notes
    }
  }

  clickTitle () {
    this.isTitleClicked = true
    this.renameTitleEvent.emit(null)
  }

  rename () {
    this.isTitleClicked = false
    const payload: IUpdateTaskDto = {
      listId: this.currentTask.listId,
      index: this.currentTask.index,
      todo: this.taskTitle
    }
    this._store.dispatch(new fromChecklist.RenameTaskAction(payload))
  }

  @HostListener('window: keyup', ['$event'])
  keyEvent (event: KeyboardEvent) {
    if (event.keyCode === KEY_CODE.ENTER) {
      if (this.isTitleClicked) {
        this.rename()
      }
    }
  }

  toggleToday () {
    const { listId, index, today } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      ['today']: !today
    }
    this._store.dispatch(new fromChecklist.ToggleMydayTaskAction(dto))
  }

  toggleToDone () {
    const { index, isDone, listId } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      ['isDone']: !isDone
    }
    this._store.dispatch(new fromChecklist.ToggleDoneTaskActin(dto))
  }

  hideDetail () {
    this.onHideDetail.emit(true)
  }

  //#region NOTES
  focusNotes (event: FocusEvent) {
    this.isNotesFocus = true
    this.isSaveNotesBtnClicked = false
    event.stopPropagation()
  }

  @HostListener('document:click') blurNotes () {
    if (this.isNotesFocus) {
      if (!this.isSaveNotesBtnClicked) {
        // 如果修改notes的确定按钮未被按下 那么就还原为原notes
        this.notes = this.currentTask.notes
      }
      this.isNotesFocus = false
    }
  }

  undoSaveNotes () {
    this.notes = this.currentTask.notes
    this.isSaveNotesBtnClicked = false
  }

  saveNotes (e: MouseEvent) {
    this.isSaveNotesBtnClicked = true
    const { listId, index } = this.currentTask
    const dto: IUpdateTaskDto = {
      listId,
      index,
      notes: this.notes
    }
    this._store.dispatch(new fromChecklist.UpdateTaskNoteAction(dto))
  }
  //#endregion

}
