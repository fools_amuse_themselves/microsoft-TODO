using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GodHM_TODO.Controllers {
  [AllowAnonymous]
  public class HomeController : Controller {
    public IActionResult Index () {
      return View ();
    }
  }
}