using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using microsoft_TODO.DTOs;
using microsoft_TODO.MongoCore;
using microsoft_TODO.ServiceCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using server.Helpers;
using microsoft_TODO.Models;

namespace microsoft_TODO.Controllers {

  [Route ("api/[controller]")]
  [ApiController]
  public class ListController : ControllerBase {

    ILogger<ListController> _logger;
    IListService _listService;

    public ListController (ILogger<ListController> logger,
      IListService listService,
      IMapper mapper) {
      _logger = logger;
      _listService = listService;
    }

    /// <summary>
    /// 获取所有清单列表
    /// </summary>
    /// <param name="uid"></param>
    /// <returns></returns>
    [HttpGet ("{uid}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(400)]
    public ActionResult<List<CheckList<TaskWithIndex>>> Get (string uid) {
      try {
        return _listService.GetAllByUserId (uid);
      } catch (Exception ex) {
        _logger.LogError(ex, "Failed to execute GET");
        return BadRequest ();
      }
    }

    /// <summary>
    /// 新建清单
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPost]
    [ProducesResponseType(201)]
    [ProducesResponseType(400)]
    public ActionResult<CheckList<Task>> Post(NewListDto dto) {
      try {
        _logger.LogInformation (LoggingEvents.InsertItem, "Inserted List {@dto} ", dto);
        var list = _listService.InsertNewList (dto);
        return Created ("", list);
      } catch (Exception ex) {
        _logger.LogError (ex, "Failed to Inserted List {@dto} ", dto);
        return BadRequest ();
      }
    }

    /// <summary>
    /// 更新清单名
    /// </summary>
    /// <param name="dto"></param>
    /// <returns></returns>
    [HttpPut ("title")]
    [ProducesResponseType(200)]
    [ProducesResponseType(400)]
    public ActionResult RenameTitle(ListTitleDto dto) {
      try {
        _listService.ModifyTitle (dto);
        _logger.LogInformation (LoggingEvents.UpdateItem, "Rename Checklist {@dto}", dto);
        return Ok();
      } catch (Exception ex) {
        _logger.LogError (ex, "Failed to Rename Checklist {@dto}", dto);
        return BadRequest ();
      }
    }

    /// <summary>
    /// 删除指定清单
    /// </summary>
    /// <param name="id">List Id</param>
    /// <returns></returns>
    [HttpDelete ("{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(400)]
    public ActionResult Delete (string id) {
      try {
        if (!_listService.IsExistent (id)) {
          _logger.LogWarning (LoggingEvents.DeleteItemNotFound, "List({ID}) Delete NOT FOUND", id);
          return NotFound (new { code = LoggingEvents.DeleteItemNotFound });
        }
        _listService.RemoveList (id);
        _logger.LogInformation (LoggingEvents.DeleteItem, "Deleted Checklist {id}", id);
        return Ok ();
      } catch (Exception ex) {
        _logger.LogError (ex, "Failed to Deleted Checklist {id}", id);
        return BadRequest ();
      }
    }
  }
}