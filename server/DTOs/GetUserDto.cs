namespace microsoft_TODO.DTOs {
    public class GetUserDto {
        public string Id { get; set; }
        public string Nickname { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
    }
}