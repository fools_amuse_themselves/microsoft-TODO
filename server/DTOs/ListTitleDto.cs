namespace microsoft_TODO.DTOs
{
    public class ListTitleDto
    {
        public string Id {get;set;}
        public string Title {get;set;}
        public string Icon {get;set;}
    }
}