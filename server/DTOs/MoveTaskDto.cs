using microsoft_TODO.Models;

namespace server.DTOs {
    public class MoveTaskDto {
        public string fromListId { get; set; }
        public int Index { get; set; }
        public string toListId { get; set; }
    }
}