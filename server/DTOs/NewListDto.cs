namespace microsoft_TODO.DTOs
{
    public class NewListDto
    {
        public string Title {get;set;}
        public string Icon {get;set;}
        public string UserId {get;set;}
    }
}