namespace microsoft_TODO.DTOs {
    public class NewTaskDto {
        public string ListId { get; set; }
        public string Todo { get; set; }
    }
}