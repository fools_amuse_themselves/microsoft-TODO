using microsoft_TODO.Models;

namespace server.DTOs {
    public class PutTaskDto : Task {
        public string ListId { get; set; }
        public int Index { get; set; }
        public bool? IsDone { get; set; }
        public bool? Today { get; set; }
    }
}