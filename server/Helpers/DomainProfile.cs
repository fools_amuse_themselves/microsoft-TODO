﻿using System.Collections.Generic;
using AutoMapper;
using microsoft_TODO.DTOs;
using microsoft_TODO.Models;
using microsoft_TODO.ViewModels;
using server.DTOs;

namespace microsoft_TODO.Helpers
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<RegisterDto, User>()
                .ForMember(u => u.Username, v => v.MapFrom(w => w.Username.ToLower()))
                .ForMember(u => u.Nickname, v => v.MapFrom(w => w.Username));
            CreateMap<User, AuthResultViewModel>();
            CreateMap<User, GetUserDto>();
            CreateMap<NewListDto, CheckList<Task>>();
            CreateMap<CheckList<Task>, CheckList<TaskWithIndex>>()
                .ForMember(u => u.Tasks, v => v.MapFrom(w => new List<TaskWithIndex>()));
            CreateMap<Task, TaskWithIndex>();
            CreateMap<MoveTaskDto, Task>();
        }
    }
}
