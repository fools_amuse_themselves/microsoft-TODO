using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace microsoft_TODO.Models
{
  public class CheckList<T>
  {
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    [BsonElement("_id")]
    public string Id { get; set; }
    [BsonElement("title")]
    public string Title { get; set; }
    [BsonElement("icon")]
    public string Icon { get; set; }
    [BsonElement("zindex")]
    public int ZIndex { get; set; }
    [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
    [BsonElement("creationTime")]
    public DateTime CreationTime { get; set; }
    [BsonElement("isStatic")]
    public bool IsStatic { get; set; }
    [BsonElement("userId")]
    [BsonRepresentation(BsonType.ObjectId)]
    public string UserId { get; set; }
    [BsonElement("tasks")]
    public System.Collections.Generic.List<T> Tasks {get;set;}

    public CheckList() {
      CreationTime = DateTime.Now;
      ZIndex =  100;
      IsStatic = false;
      Icon = "bars";
      Tasks = new System.Collections.Generic.List<T>();
    }
  }
}