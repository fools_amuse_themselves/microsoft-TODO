﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace microsoft_TODO.Models
{
    [BsonIgnoreExtraElements]
    public class RefreshToken
    {
        [BsonElement("userid")]
        public string UserId { get; set; }
        [BsonElement("refresh_date")]
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime RefreshDate { get; set; }
        [BsonElement("token")]
        public string Token { get; set; }
    }
}
