using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace microsoft_TODO.Models {
    public class Task {
        public Task () {
            IsDone = false;
            Today = false;
            CreationTime = DateTime.Now;
        }

        [BsonElement ("todo")]
        public string Todo { get; set; }
        [BsonElement ("isDone")]
        public bool IsDone { get; set; }
        [BsonElement ("today")]
        public bool Today { get; set; }
        [BsonDateTimeOptions (Kind = DateTimeKind.Local)]
        [BsonElement ("creationTime")]
        public DateTime CreationTime { get; set; }
        [BsonDateTimeOptions (Kind = DateTimeKind.Local)]
        [BsonElement ("reminderTime")]
        public DateTime? ReminderTime { get; set; }
        [BsonDateTimeOptions (Kind = DateTimeKind.Local)]
        [BsonElement ("deadline")]
        public DateTime? Deadline { get; set; }
        [BsonElement ("notes")]
        public string Notes { get; set; }
    }
}