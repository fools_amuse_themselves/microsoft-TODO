using microsoft_TODO.Models;

namespace microsoft_TODO.Models
{
    public class TaskWithIndex: Task
    {
        public int Index {get;set;}
    }
}