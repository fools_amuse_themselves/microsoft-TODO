using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace microsoft_TODO.Models {
    public class User {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonElement("_id")]
        public string Id { get; set; }
        [BsonElement("username")]
        public string Username { get; set; }
        [BsonElement("password")]
        public string Password { get; set; }
        [BsonElement("nickname")]
        public string Nickname { get; set; }
        [BsonElement("avatar")]
        public string Avatar { get; set; }
        [BsonElement("email")]
        public string Email { get; set; }
        [BsonElement("logonTime")]
        [BsonDateTimeOptions (Kind = DateTimeKind.Local)]
        public DateTime LogonTime {get;set;}
    }
}