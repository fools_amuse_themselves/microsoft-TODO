using System.Collections.Generic;
using microsoft_TODO.Models;
using MongoDB.Bson;

namespace microsoft_TODO.MongoCore {
    public interface IListRepository: IRepository<CheckList<Task>> {
        bool RemoveTaskViaIndexOfArray(string lid, int index);

        bool UpdateTaskViaIndexOfArray(string lid, int index, string prop, BsonValue value);
    }
}