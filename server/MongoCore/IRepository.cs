﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace microsoft_TODO.MongoCore
{
    public interface IRepository<TEntity>
    {
        TEntity Insert(TEntity entity);
        TEntity FirstOrDefault(Expression<Func<TEntity, bool>> exp);
        IQueryable<TEntity> GetAll();
        List<TEntity> GetAllList(Expression<Func<TEntity, bool>> exp = null);
        void Update(Expression<Func<TEntity, bool>> exp, UpdateDefinition<TEntity> update);
        void Upsert(Expression<Func<TEntity, bool>> exp, UpdateDefinition<TEntity> update);
        bool Delete(Expression<Func<TEntity, bool>> exp);
        long Count(Expression<Func<TEntity, bool>> exp);
        bool IsExistent(Expression<Func<TEntity, bool>> exp);
    }
}
