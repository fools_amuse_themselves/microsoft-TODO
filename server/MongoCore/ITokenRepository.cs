﻿using microsoft_TODO.Models;

namespace microsoft_TODO.MongoCore
{
    public interface ITokenRepository : IRepository<RefreshToken>
    {
    }
}
