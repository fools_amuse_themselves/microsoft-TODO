using System.Collections.Generic;
using microsoft_TODO.Models;

namespace microsoft_TODO.MongoCore
{
  public interface IUserRepository : IRepository<User>
  {
  }
}