﻿using Microsoft.Extensions.Configuration;
using microsoft_TODO.Models;

namespace microsoft_TODO.MongoCore.Repositories
{
    public class TokenRepository : BaseRepository<RefreshToken>, ITokenRepository
    {
        public TokenRepository(IConfiguration configuration) : base(configuration, "refresh_token")
        {
        }
    }
}
