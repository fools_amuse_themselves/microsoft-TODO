﻿using Microsoft.Extensions.Configuration;
using microsoft_TODO.Models;

namespace microsoft_TODO.MongoCore.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(IConfiguration configuration) : base(configuration, "user") { }
    }
}
