﻿using System.IO;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace microsoft_TODO
{
  public class Program
  {
    public static void Main(string[] args)
    {
      CreateWebHostBuilder(args).Build().Run();
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseKestrel()
            .UseContentRoot(Directory.GetCurrentDirectory())
            //.UseIISIntegration()
            .ConfigureLogging((hostingContext, builder) =>
            {
              var configration = hostingContext.Configuration.GetSection("Logging");
              builder.AddFile(configration);
            })
            // .UseUrls("http://localhost:5000")
            .UseStartup<Startup>();
  }
}