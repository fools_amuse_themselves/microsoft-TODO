using System.Collections.Generic;
using microsoft_TODO.DTOs;
using microsoft_TODO.Models;

namespace microsoft_TODO.ServiceCore {
    public interface IListService {
        List<CheckList<TaskWithIndex>> GetAllByUserId (string uid);

        CheckList<Task> InsertNewList (NewListDto dto);

        bool IsExistent (string id);

        bool RemoveList (string id);

        void ModifyTitle (ListTitleDto dto);
    }
}