using microsoft_TODO.DTOs;
using microsoft_TODO.Models;
using microsoft_TODO.MongoCore;
using server.DTOs;
using System.Collections.Generic;

namespace microsoft_TODO.ServiceCore {
    public interface ITaskService {
        void TurnTodayTasksToFalse(string uid);

        Task AddTodayTask (NewTaskDto dto);

        Task AddNewTask (NewTaskDto dto);

        void MoveToAnotherList (MoveTaskDto dto);

        bool RemoveTask (string lid, int index);

        bool UpdateTask (PutTaskDto dto);

        bool ClearTaskDeadline (string lid, int index);
    }
}