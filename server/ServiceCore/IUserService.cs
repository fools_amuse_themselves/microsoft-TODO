﻿using microsoft_TODO.DTOs;
using microsoft_TODO.Models;

namespace microsoft_TODO.ServiceCore
{
    public interface IUserService
    {
        /// <summary>
        /// 用户登录验证
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        User Authenticate(string username, string password);

        /// <summary>
        /// 插入或更新 refresh token
        /// </summary>
        /// <param name="token">refresh token entity</param>
        /// <returns></returns>
        void UpsertRefreshToken(RefreshToken token);

        /// <summary>
        /// 判断 refresh token 是否过期
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="token">refresh token</param>
        /// <returns></returns>
        bool IsExpiredRefreshToken(string userId, string token);

        /// <summary>
        /// 注册新用户
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        bool CreateUser(RegisterDto dto);

        /// <summary>
        /// 用户名是否存在
        /// </summary>
        /// <param name="username">usernmae</param>
        /// <returns></returns>
        bool IsUsernameExistent(string username);

        /// <summary>
        /// 根据用户ID获取用户信息
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns></returns>
        User GetUserById(string id);

        /// <summary>
        /// 更新登录时间
        /// </summary>
        /// <param name="id">user id</param>
        void UpdateLogonTime(string id);
    }
}
