﻿namespace microsoft_TODO.ViewModels
{
    public class AuthResultViewModel
    {
        public string Id { get; set; }
        public string Nickname { get; set; }
        public string Avatar { get; set; }
        public string Email { get; set; }
        public TokenResult Token { get; set; }
    }

    public class TokenResult
    {
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public long expires_in { get; set; }
    }
}
