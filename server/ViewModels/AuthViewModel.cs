﻿namespace microsoft_TODO.ViewModels
{
    public class AuthViewModel
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string GrantType { get; set; }
        public string RefreshToken { get; set; }
    }
}
